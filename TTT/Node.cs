﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TTT
{
    public class Node<T>
    {
        public List<Node<T>> filhos;
        public string valor;
        public int score;
        //public bool ganhador;
        //public filhos = new List<Node<T>>();
        public Node()
        {
            filhos = new List<Node<T>>();
        }   

        public override string ToString()
        {
            return string.Format(   "{0} | {1} | {2}" + System.Environment.NewLine + 
                                    "{3} | {4} | {5}" + System.Environment.NewLine + 
                                    "{6} | {7} | {8}", 
                                    valor[0], valor[1], valor[2], valor[3], valor[4], valor[5], valor[6], valor[7], valor[8]);
        }
        
    }
    
}
