﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TTT;



namespace TicTacToe
{
    static class Program
    {
        #region ReplaceAt
        public static string ReplaceAt(this string str, int index, int length, string replace)
        {
            return str.Remove(index, Math.Min(length, str.Length - index))
                    .Insert(index, replace);
        }
        #endregion

        public static void Play(char player, Node<string> game, char originalPlayer)
        {
            //if (!game.valor.Contains('_'))
            if (Winner(game))
            {
                return;
            }


            for (int i = 0; i < 9; i++)
            {
                var jogada = game.valor;
                if (game.valor[i].Equals('_'))
                {
                    jogada = ReplaceAt(game.valor, i, 1, player.ToString());

                    var child = new Node<string>() { valor = jogada };
                    game.filhos.Add(child);
                    Play(player.Equals('x') ? 'o' : 'x', child, originalPlayer);
                    game.score = MiniMax(game, player, originalPlayer);
                }
            }


        }

        public static int Score(Node<string> estado, char player)
        {
            if ((estado.valor[0] == estado.valor[1] && estado.valor[1] == estado.valor[2] && estado.valor[2] == player) || //horizontal
                (estado.valor[3] == estado.valor[4] && estado.valor[4] == estado.valor[5] && estado.valor[5] == player) ||
                (estado.valor[6] == estado.valor[7] && estado.valor[7] == estado.valor[8] && estado.valor[8] == player) ||

                (estado.valor[0] == estado.valor[4] && estado.valor[4] == estado.valor[8] && estado.valor[8] == player) || //diagonais
                (estado.valor[2] == estado.valor[4] && estado.valor[4] == estado.valor[6] && estado.valor[6] == player) ||

                (estado.valor[0] == estado.valor[3] && estado.valor[3] == estado.valor[6] && estado.valor[6] == player) ||//verticais
                (estado.valor[1] == estado.valor[4] && estado.valor[4] == estado.valor[7] && estado.valor[7] == player) ||
                (estado.valor[2] == estado.valor[5] && estado.valor[5] == estado.valor[8] && estado.valor[8] == player))
                return 1;
            return -1;
        }

        public static bool Winner(Node<string> estado)
        {

            if ((estado.valor[0] == estado.valor[1] && estado.valor[1] == estado.valor[2] && estado.valor[2] != '_') || //horizontal
                (estado.valor[3] == estado.valor[4] && estado.valor[4] == estado.valor[5] && estado.valor[5] != '_') ||
                (estado.valor[6] == estado.valor[7] && estado.valor[7] == estado.valor[8] && estado.valor[8] != '_') ||

                (estado.valor[0] == estado.valor[4] && estado.valor[4] == estado.valor[8] && estado.valor[8] != '_') || //diagonais
                (estado.valor[2] == estado.valor[4] && estado.valor[4] == estado.valor[6] && estado.valor[6] != '_') ||

                (estado.valor[0] == estado.valor[3] && estado.valor[3] == estado.valor[6] && estado.valor[6] != '_') ||//verticais
                (estado.valor[1] == estado.valor[4] && estado.valor[4] == estado.valor[7] && estado.valor[7] != '_') ||
                (estado.valor[2] == estado.valor[5] && estado.valor[5] == estado.valor[8] && estado.valor[8] != '_'))
                return true;
            return false;
        }

        public static bool Draw(Node<string> estado, char player)
        {
            if (!estado.valor.Contains('_') && Winner(estado) == false)
            {
                return true;
            }
            return false;
        }

        public static int MiniMax(Node<string> node, char player, char originalPlayer)
        {
            if (node.filhos.Count == 0 || Winner(node))
                return Score(node, player);
            if (player == originalPlayer)
            {
                var a = 0;
                foreach (var f in node.filhos)
                {
                    a = a + MiniMax(f, player, originalPlayer);
                }
                return a;
            }
            else
            {
                var a = 0;
                foreach (var f in node.filhos)
                {
                    a = a - MiniMax(f, player, originalPlayer);
                }
                return a;
            }
        }
        // http://snipd.net/minimax-algorithm-with-alpha-beta-pruning-in-c

        public static Node<string> AvaliaScore(Node<string> node, char player, char originalPlayer)
        {

                var f = new Node<string>();
                foreach (var item in node.filhos)
                {
                    if (item.score > f.score)
                    {
                        f = item;
                    }
                }

                return f;
        }

        public static Node<string> EscolherMelhorResultado(Node<string> node, char player, char originalPlayer)
        {
            var nodeatual = new Node<string>();
            if (AvaliaScore.f )
            {
                
            }
        }

        //#region test

        //public static MinMax(Node<string> node)
        //{
        //    if (node.filhos.Count == 0 || node.score != 0)
        //        return estado.valor;
        //    else 
        //    {
        //        var a = 0;
        //        foreach (var f in node.filhos)
        //        {
        //            a = node.filhos ==  ? a + MinMax(f) : a - MinMax(f);
        //        }
        //        return a;
        //    }
        //}

        //public static MiniMax()
        //{
            
        //}

        //public void pontos(Node<string> node)
        //{
        //    foreach (var i in node.filhos   )
        //    {
        //        if (i.ganhador == null && i.filhos.Count != 0)
        //            pontuar(i);
        //    }
        //    if (node.filhos.Count == 0)
        //    {
        //        if (node.ganhador == null)
                    
        //        return;
        //    }
        //}
        //#endregion

        #region DESATIVADO
        #region (desativado)Pontuação

        //public static void Pontuacao(char player, Node<string> game)
        //{
        //    if (player == 'x')
        //    {
        //        if (!game.valor.Any(s => s.Equals('_')))
        //        {
        //            #region AIPlay
        //            if ((game.valor[0] == game.valor[1] && game.valor[1] == game.valor[2] && game.valor[2] != '_') &&
        //                (game.valor[3] == game.valor[4] && game.valor[4] == game.valor[5] && game.valor[5] != '_') &&
        //                (game.valor[6] == game.valor[7] && game.valor[7] == game.valor[8] && game.valor[8] != '_') &&
        //                (game.valor[0] == game.valor[3] && game.valor[3] == game.valor[6] && game.valor[6] != '_') &&
        //                (game.valor[1] == game.valor[4] && game.valor[4] == game.valor[7] && game.valor[7] != '_') &&
        //                (game.valor[2] == game.valor[5] && game.valor[5] == game.valor[8] && game.valor[8] != '_') &&
        //                (game.valor[0] == game.valor[4] && game.valor[4] == game.valor[8] && game.valor[8] != '_') &&
        //                (game.valor[2] == game.valor[4] && game.valor[4] == game.valor[6] && game.valor[6] != '_'))
        //            {
        //                game.score = 1;
        //            }
        //            #endregion
        //            #region NotAIPlay
        //            if ((game.valor[0] == game.valor[1] && game.valor[1] == game.valor[2] && game.valor[2] != '_') &&
        //                (game.valor[3] == game.valor[4] && game.valor[4] == game.valor[5] && game.valor[5] != '_') &&
        //                (game.valor[6] == game.valor[7] && game.valor[7] == game.valor[8] && game.valor[8] != '_') &&
        //                (game.valor[0] == game.valor[3] && game.valor[3] == game.valor[6] && game.valor[6] != '_') &&
        //                (game.valor[1] == game.valor[4] && game.valor[4] == game.valor[7] && game.valor[7] != '_') &&
        //                (game.valor[2] == game.valor[5] && game.valor[5] == game.valor[8] && game.valor[8] != '_') &&
        //                (game.valor[0] == game.valor[4] && game.valor[4] == game.valor[8] && game.valor[8] != '_') &&
        //                (game.valor[2] == game.valor[4] && game.valor[4] == game.valor[6] && game.valor[6] != '_'))
        //            {
        //                game.score = -1;
        //            }
        //            #region Draw
        //            else
        //            {
        //                game.score = 0;
        //            }
        //            #endregion
        //        }
        //#endregion
        //    }
        //}
        #endregion
        #region (desativado)ReplaceFirst
        //static string ReplaceFirst(string text, int index, string replace)
        //{
        //    int pos = text[index];
        //    if (pos < 0)
        //    {
        //        return text;
        //    }
        //    return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        //}
        #endregion
        #region (desativado)Vencer
        //public static bool Vencer(Node<string> estado, char player)
        //{
        //    List<string> vitX = new List<string>() {"xxx______","___xxx___","______xxx",
        //                                           "x___x___x","__x_x___x",
        //                                           "x__x___x_","_x__x__x_","__x__x__x"};

        //    List<string> vitO = new List<string>() {"ooo______","___ooo___","______ooo",
        //                                           "o___o___o","__o_o___o",
        //                                           "o__o___o_","_o__o__o_","__o__o__o"};

        //    StringBuilder b = new StringBuilder(estado.valor);

        //    b.Replace(player.Equals('x') ? 'o' : 'x', '_');

        //    if (b.Equals(vitX) || b.Equals(vitO))
        //        return true;
        //    return false;
        //}

        //public static bool Velha(Node<string> estado, char player)
        //{
        //    if (!estado.valor.Contains('_') && Vencer(estado, player) == false)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        //public static int Score(Node<string> estado, char player)
        //{
        //    if (Vencer(estado, player))
        //    {
        //        return 1000;
        //    }
        //    else if (Vencer(estado, player.Equals('x') ? 'o' : 'x'))
        //    {
        //        return -1000;
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}
        #endregion
        #region (desativado)GetEstado
        //public static void GetEstado(string board, char player)
        //    {
        //    int contadorPlayer = 0, contador_ = 0, contadorNotPlayer = 0;
        //    char notPlayer = player.Equals('x') ? 'o' : 'x';

        //    #region Contador da primeira linha
        //    for (var i = 0; i < 3; i++)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contador_ = 0;
        //    contadorNotPlayer = 0;
        //    contadorPlayer = 0;
        //    #endregion

        //    #region Contador da segunda linha
        //    for (var i = 3; i < 6; i++)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contador_ = 0;
        //    contadorNotPlayer = 0;
        //    contadorPlayer = 0;
        //    #endregion

        //    #region Contador da terceira linha
        //    for (var i = 6; i < 9; i++)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contador_ = 0;
        //    contadorNotPlayer = 0;
        //    contadorPlayer = 0;
        //    #endregion

        //    #region Contador da primeira coluna
        //    for (var i = 0; i < 6; i += 3)
        //    {
        //        if(board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contadorPlayer = 0;
        //    contadorNotPlayer = 0;
        //    contador_ = 0;
        //    #endregion

        //    #region Contador da segunda coluna
        //    for (var i = 1; i < 7; i += 3)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contadorPlayer = 0;
        //    contadorNotPlayer = 0;
        //    contador_ = 0;
        //    #endregion

        //    #region Contador da terceira coluna
        //    for (var i = 2; i < 8; i += 3)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contadorPlayer = 0;
        //    contadorNotPlayer = 0;
        //    contador_ = 0;
        //    #endregion

        //    #region Contador da primeira diagonal
        //    for (var i = 0; i < 8; i += 4)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contadorPlayer = 0;
        //    contadorNotPlayer = 0;
        //    contador_ = 0;
        //    #endregion

        //    #region Contador da segunda diagonal
        //    for (var i = 2; i < 6; i += 2)
        //    {
        //        if (board[i] == player)
        //            contadorPlayer++;
        //        if (board[i] == notPlayer)
        //            contadorNotPlayer++;
        //        if (board[i] == '_')
        //            contador_++;
        //    }

        //    Score(contadorPlayer, contadorNotPlayer, contador_);

        //    contadorPlayer = 0;
        //    contadorNotPlayer = 0;
        //    contador_ = 0;
            #endregion
        #region (desativado)Score
        public static int Score(int contadorPlayer, int contadorNotPlayer, int contador_)
        {
            int score = 0;

            if (contadorPlayer == 3)
                score = 10000;
            if (contadorPlayer == 2)
                score = 1000;
            if (contadorPlayer == 1 && contadorNotPlayer == 1)
                score = 0;
            if (contador_ == 3)
                score = 10;
            if (contadorNotPlayer == 2)
                score = -1000;

            return score;
        }
        #endregion
        #endregion

        #region MAIN
        static void Main(string[] args)
        {

            #region variaveis

            var game = new Node<string>() { filhos = new List<Node<string>>(), valor = "_________" };
            var game1 = new Node<string>() { filhos = new List<Node<string>>(), valor = "xxxoo____" }; //teste
            char player = 'x';
            
            #endregion

            Play(player, game, player);

            string strx = game.ToString();//string override
            
            //Console.WriteLine(strx); 

            //game.filhos.ForEach(Console.WriteLine);//escrever filhos console
            
            Console.ReadKey();

        }
        #endregion
    }

}